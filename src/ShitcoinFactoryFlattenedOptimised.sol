// SPDX-License-Identifier: MIT
pragma solidity ^0.8.24;

interface IUniswapV2Factory {
    function createPair(address tokenA, address tokenB)
        external
        returns (address pair);
}

interface IUniswapV2Router02 {
    function WETH() external view returns (address);

    function addLiquidityETH(
        address token,
        uint256 amountTokenDesired,
        uint256 amountTokenMin,
        uint256 amountETHMin,
        address to,
        uint256 deadline
    )
        external
        payable
        returns (
            uint256 amountToken,
            uint256 amountETH,
            uint256 liquidity
        );

    function factory() external view returns (address);

    function swapExactTokensForETHSupportingFeeOnTransferTokens(
        uint256 amountIn,
        uint256 amountOutMin,
        address[] memory path,
        address to,
        uint256 deadline
    ) external;
}

/// @notice Modern and gas efficient ERC20 + EIP-2612 implementation.
/// @author Solmate (https://github.com/transmissions11/solmate/blob/main/src/tokens/ERC20.sol)
/// @author Modified from Uniswap (https://github.com/Uniswap/uniswap-v2-core/blob/master/contracts/UniswapV2ERC20.sol)
/// @dev Do not manually set balances without updating totalSupply, as the sum of all user balances must not exceed it.
abstract contract ERC20 {
    /*//////////////////////////////////////////////////////////////
                                 EVENTS
    //////////////////////////////////////////////////////////////*/

    event Transfer(address indexed from, address indexed to, uint256 amount);

    event Approval(
        address indexed owner,
        address indexed spender,
        uint256 amount
    );

    /*//////////////////////////////////////////////////////////////
                            METADATA STORAGE
    //////////////////////////////////////////////////////////////*/

    string public name;

    string public symbol;

    uint8 public immutable decimals;

    /*//////////////////////////////////////////////////////////////
                              ERC20 STORAGE
    //////////////////////////////////////////////////////////////*/

    uint256 public totalSupply;

    mapping(address => uint256) public balanceOf;

    mapping(address => mapping(address => uint256)) public allowance;

    /*//////////////////////////////////////////////////////////////
                            EIP-2612 STORAGE
    //////////////////////////////////////////////////////////////*/

    uint256 internal immutable INITIAL_CHAIN_ID;

    bytes32 internal immutable INITIAL_DOMAIN_SEPARATOR;

    mapping(address => uint256) public nonces;

    /*//////////////////////////////////////////////////////////////
                               CONSTRUCTOR
    //////////////////////////////////////////////////////////////*/

    constructor(
        string memory _name,
        string memory _symbol,
        uint8 _decimals
    ) {
        name = _name;
        symbol = _symbol;
        decimals = _decimals;

        INITIAL_CHAIN_ID = block.chainid;
        INITIAL_DOMAIN_SEPARATOR = computeDomainSeparator();
    }

    /*//////////////////////////////////////////////////////////////
                               ERC20 LOGIC
    //////////////////////////////////////////////////////////////*/

    function approve(address spender, uint256 amount)
        public
        virtual
        returns (bool)
    {
        allowance[msg.sender][spender] = amount;

        emit Approval(msg.sender, spender, amount);

        return true;
    }

    function _transfer(
        address _from,
        address _to,
        uint256 _amount
    ) internal virtual returns (bool) {
        balanceOf[_from] -= _amount;

        // Cannot overflow because the sum of all user
        // balances can't exceed the max uint256 value.
        unchecked {
            balanceOf[_to] += _amount;
        }

        emit Transfer(_from, _to, _amount);

        return true;
    }

    function transfer(address to, uint256 amount)
        public
        virtual
        returns (bool)
    {
        bool success = _transfer(msg.sender, to, amount);
        require(success, "transfer failed");
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) public virtual returns (bool) {
        uint256 allowed = allowance[from][msg.sender]; // Saves gas for limited approvals.

        if (allowed != type(uint256).max)
            allowance[from][msg.sender] = allowed - amount;

        bool success = _transfer(from, to, amount);
        require(success, "transfer failed");

        return true;
    }

    /*//////////////////////////////////////////////////////////////
                             EIP-2612 LOGIC
    //////////////////////////////////////////////////////////////*/

    function permit(
        address owner,
        address spender,
        uint256 value,
        uint256 deadline,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public virtual {
        require(deadline >= block.timestamp, "PERMIT_DEADLINE_EXPIRED");

        // Unchecked because the only math done is incrementing
        // the owner's nonce which cannot realistically overflow.
        unchecked {
            address recoveredAddress = ecrecover(
                keccak256(
                    abi.encodePacked(
                        "\x19\x01",
                        DOMAIN_SEPARATOR(),
                        keccak256(
                            abi.encode(
                                keccak256(
                                    "Permit(address owner,address spender,uint256 value,uint256 nonce,uint256 deadline)"
                                ),
                                owner,
                                spender,
                                value,
                                nonces[owner]++,
                                deadline
                            )
                        )
                    )
                ),
                v,
                r,
                s
            );

            require(
                recoveredAddress != address(0) && recoveredAddress == owner,
                "INVALID_SIGNER"
            );

            allowance[recoveredAddress][spender] = value;
        }

        emit Approval(owner, spender, value);
    }

    function DOMAIN_SEPARATOR() public view virtual returns (bytes32) {
        return
            block.chainid == INITIAL_CHAIN_ID
                ? INITIAL_DOMAIN_SEPARATOR
                : computeDomainSeparator();
    }

    function computeDomainSeparator() internal view virtual returns (bytes32) {
        return
            keccak256(
                abi.encode(
                    keccak256(
                        "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)"
                    ),
                    keccak256(bytes(name)),
                    keccak256("1"),
                    block.chainid,
                    address(this)
                )
            );
    }

    /*//////////////////////////////////////////////////////////////
                        INTERNAL MINT/BURN LOGIC
    //////////////////////////////////////////////////////////////*/

    function _mint(address to, uint256 amount) internal virtual {
        totalSupply += amount;

        // Cannot overflow because the sum of all user
        // balances can't exceed the max uint256 value.
        unchecked {
            balanceOf[to] += amount;
        }

        emit Transfer(address(0), to, amount);
    }

    function _burn(address from, uint256 amount) internal virtual {
        balanceOf[from] -= amount;

        // Cannot underflow because a user's balance
        // will never be larger than the total supply.
        unchecked {
            totalSupply -= amount;
        }

        emit Transfer(from, address(0), amount);
    }
}

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * The initial owner is set to the address provided by the deployer. This can
 * later be changed with {transferOwnership}.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be applied to your functions to restrict their use to
 * the owner.
 */
abstract contract Ownable {
    address private _owner;

    /**
     * @dev The caller account is not authorized to perform an operation.
     */
    error OwnableUnauthorizedAccount(address account);

    /**
     * @dev The owner is not a valid owner account. (eg. `address(0)`)
     */
    error OwnableInvalidOwner(address owner);

    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );

    /**
     * @dev Initializes the contract setting the address provided by the deployer as the initial owner.
     */
    constructor(address initialOwner) {
        if (initialOwner == address(0)) {
            revert OwnableInvalidOwner(address(0));
        }
        _transferOwnership(initialOwner);
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        _checkOwner();
        _;
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if the sender is not the owner.
     */
    function _checkOwner() internal view virtual {
        if (owner() != msg.sender) {
            revert OwnableUnauthorizedAccount(msg.sender);
        }
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby disabling any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        if (newOwner == address(0)) {
            revert OwnableInvalidOwner(address(0));
        }
        _transferOwnership(newOwner);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Internal function without access restriction.
     */
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}

// Our custom token
contract Shitcoin is ERC20, Ownable {
    /*//////////////////////////////////////////////////////////////
                               Constants
    //////////////////////////////////////////////////////////////*/

    uint8 internal constant SF_TXN_TAX = 1; // 1% of each transaction goes to the Shitcoin Factory

    /*//////////////////////////////////////////////////////////////
                               Variables
    //////////////////////////////////////////////////////////////*/
    address internal taxRecipient; // Address that receives the tax

    IUniswapV2Router02 public uniswapV2Router;
    address public uniswapV2Pair;

    uint8 public txnTax; // Tax per transaction (excluding 1% Shitcoin Factory tax)
    uint256 public maxTransactionAmount;
    uint256 public maxWallet;
    uint256 public swapTokensAtAmount;

    bool public limitsInEffect;

    bool private swapping;

    // exclude from fees and max transaction amount
    mapping(address => bool) public isExcludedFromFeesAndMaxTransaction;

    /*//////////////////////////////////////////////////////////////
                                Events
    //////////////////////////////////////////////////////////////*/
    event ExcludeFromFees(address indexed account, bool isExcluded);

    event SetAutomatedMarketMakerPair(address indexed pair, bool indexed value);

    event UpdateUniswapV2Router(
        address indexed newAddress,
        address indexed oldAddress
    );

    event teamWalletUpdated(
        address indexed newWallet,
        address indexed oldWallet
    );

    constructor(
        address _uniswapV2Router, // Uniswapv2 compatible router
        string memory _name,
        string memory _symbol,
        uint256 _totalSupply, // Total supply of Token
        uint256 _lockedLiquidity, // Amount of tokens (to be locked into liquidity)
        uint8 _txnTax, // Tax on each transaction
        uint8 _maxTxn, // max txn as a percentage of total supply
        uint8 _maxPos, // max wallet as a percentage of total supply
        bool _limits, // whether or not to apply limits
        address _deployer, // person who wants to deploy the token
        address _taxRecipient // address that receives the 1% tax
    ) ERC20(_name, _symbol, 18) Ownable(_deployer) {
        uniswapV2Router = IUniswapV2Router02(_uniswapV2Router);
        uniswapV2Pair = IUniswapV2Factory(uniswapV2Router.factory()).createPair(
            address(this),
            uniswapV2Router.WETH()
        );

        // exclude from paying fees or having max transaction amount
        excludeFromFeesAndMaxTransaction(_uniswapV2Router, true);
        excludeFromFeesAndMaxTransaction(address(uniswapV2Pair), true);

        // set variables
        txnTax = _txnTax;
        taxRecipient = msg.sender;
        maxTransactionAmount = (_totalSupply * _maxTxn) / 100;
        maxWallet = (_totalSupply * _maxPos) / 100;
        taxRecipient = _taxRecipient;
        swapTokensAtAmount = (totalSupply * 5) / 10000; // 0.05%
        limitsInEffect = _limits;

        // exclude from paying fees or having max transaction amount
        excludeFromFeesAndMaxTransaction(_deployer, true);
        excludeFromFeesAndMaxTransaction(address(this), true);
        excludeFromFeesAndMaxTransaction(address(msg.sender), true);

        // Mint to msg.sender (Shitcoin Factory) for liquidity
        _mint(msg.sender, _lockedLiquidity);

        // Deployer keeps the rest
        uint256 deployerAmount = _totalSupply - _lockedLiquidity;
        if (deployerAmount > 0) {
            _mint(_deployer, deployerAmount);
        }
    }

    receive() external payable {}

    function _transfer(
        address from,
        address to,
        uint256 amount
    ) internal override returns (bool) {
        if (amount == 0) {
            super._transfer(from, to, 0);
            return true;
        }

        // maxTxn & maxWallet
        if (limitsInEffect) {
            if (
                from != owner() &&
                to != owner() &&
                to != address(0) &&
                to != address(0x42) &&
                !swapping
            ) {
                //when buy
                if (
                    from == uniswapV2Pair &&
                    !isExcludedFromFeesAndMaxTransaction[to]
                ) {
                    require(
                        amount <= maxTransactionAmount,
                        "Buy transfer amount exceeds the maxTransactionAmount."
                    );
                    require(
                        amount + balanceOf[to] <= maxWallet,
                        "Max wallet exceeded"
                    );
                }
                //when sell
                else if (
                    to == uniswapV2Pair &&
                    !isExcludedFromFeesAndMaxTransaction[from]
                ) {
                    require(
                        amount <= maxTransactionAmount,
                        "Sell transfer amount exceeds the maxTransactionAmount."
                    );
                } else if (isExcludedFromFeesAndMaxTransaction[to]) {
                    require(
                        amount + balanceOf[to] <= maxWallet,
                        "Max wallet exceeded"
                    );
                }
            }
        }

        // Swap tokens for ETH
        uint256 contractTokenBalance = balanceOf[address(this)];

        bool canSwap = contractTokenBalance >= swapTokensAtAmount;

        if (
            canSwap &&
            !swapping &&
            from != uniswapV2Pair &&
            !isExcludedFromFeesAndMaxTransaction[from] &&
            !isExcludedFromFeesAndMaxTransaction[to]
        ) {
            swapping = true;

            swapBack();

            swapping = false;
        }

        bool takeFee = !swapping;

        // if any account belongs to _isExcludedFromFee account then remove the fee
        if (
            isExcludedFromFeesAndMaxTransaction[from] ||
            isExcludedFromFeesAndMaxTransaction[to]
        ) {
            takeFee = false;
        }

        uint256 fees = 0;
        // only take fees on buys/sells, do not take on wallet transfers
        if (takeFee) {
            // on sell
            if (to == uniswapV2Pair || from == uniswapV2Pair) {
                fees = (amount * (SF_TXN_TAX + txnTax)) / (100);
            }

            if (fees > 0) {
                super._transfer(from, address(this), fees);
            }

            amount -= fees;
        }

        // Transfer the amount after tax
        return super._transfer(from, to, amount);
    }

    function swapBack() private returns (bool) {
        uint256 contractBalance = balanceOf[address(this)];

        if (contractBalance == 0) {
            return true;
        }

        swapTokensForEth(contractBalance);

        uint256 ethBalance = address(this).balance;
        // split between protocol and team wallet
        uint256 onePercentTax = ethBalance / (SF_TXN_TAX + txnTax);

        (bool successOne, ) = address(taxRecipient).call{value: onePercentTax}(
            ""
        );

        (bool successTwo, ) = address(owner()).call{
            value: address(this).balance
        }("");

        return (successOne && successTwo);
    }

    function swapTokensForEth(uint256 tokenAmount) private {
        // generate the uniswap pair path of token -> weth
        address[] memory path = new address[](2);
        path[0] = address(this);
        path[1] = uniswapV2Router.WETH();

        approve(address(uniswapV2Router), tokenAmount);

        // make the swap
        uniswapV2Router.swapExactTokensForETHSupportingFeeOnTransferTokens(
            tokenAmount,
            0, // accept any amount of ETH
            path,
            address(this),
            block.timestamp
        );
    }

    function withdrawStuckShitcoin() external onlyOwner {
        transfer(msg.sender, balanceOf[address(this)]);
    }

    function withdrawStuckEth(address toAddr) external onlyOwner {
        (bool success, ) = toAddr.call{value: address(this).balance}("");
        require(success);
    }

    // remove limits after token is stable
    function removeLimits() external onlyOwner returns (bool) {
        limitsInEffect = false;
        return true;
    }

    // change the minimum amount of tokens to sell from fees
    function updateSwapTokensAtAmount(uint256 newAmount)
        external
        onlyOwner
        returns (bool)
    {
        require(
            newAmount >= (totalSupply * 1) / 100000,
            "Swap amount cannot be lower than 0.001% total supply."
        );
        require(
            newAmount <= (totalSupply * 5) / 1000,
            "Swap amount cannot be higher than 0.5% total supply."
        );
        swapTokensAtAmount = newAmount;
        return true;
    }

    function updateMaxTxnAmount(uint256 newNum) external onlyOwner {
        require(
            newNum >= ((totalSupply * 5) / 1000) / 1e18,
            "Cannot set maxTransactionAmount lower than 0.5%"
        );
        maxTransactionAmount = newNum * (10**18);
    }

    function updateMaxWalletAmount(uint256 newNum) external onlyOwner {
        require(
            newNum >= ((totalSupply * 10) / 1000) / 1e18,
            "Cannot set maxWallet lower than 1.0%"
        );
        maxWallet = newNum * (10**18);
    }

    function excludeFromFeesAndMaxTransaction(address updAds, bool isEx)
        public
        onlyOwner
    {
        isExcludedFromFeesAndMaxTransaction[updAds] = isEx;
    }

    function updateTxnTax(uint8 _txnTax) external onlyOwner {
        require(_txnTax <= 9, "Cannot set txnTax higher than 9%");
        txnTax = _txnTax;
    }
}

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the value of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves a `value` amount of tokens from the caller's account to `to`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address to, uint256 value) external returns (bool);

    /**
     * @dev Sets a `value` amount of tokens as the allowance of `spender` over the
     * caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 value) external returns (bool);
}

contract ShitcoinFactory is Ownable {
    /*//////////////////////////////////////////////////////////////
                               Constants
    //////////////////////////////////////////////////////////////*/

    /*//////////////////////////////////////////////////////////////
                               Variables
    //////////////////////////////////////////////////////////////*/

    address public taxRecipient = 0x65c2156DddC5808f10f6656f0aF50c76F4D4b7C3; // Turbo

    uint256 public deployerFee = 0.02 ether; // 0.02 ETH

    uint8 public maxTxnTax = 9; // 9% max tax

    uint8 public minLiquidity = 30; // at least 30% of the max supply must be locked in liquidity

    /*//////////////////////////////////////////////////////////////
                                Events
    //////////////////////////////////////////////////////////////*/

    // Deployment
    event NewToken(
        address indexed token,
        uint256 totalSupply,
        uint256 lockedLiquidity,
        address indexed deployer,
        uint256 indexed initialEthLiquidity
    );

    constructor() Ownable(msg.sender) {}

    function deploy(
        address _uniV2Router, // Uniswapv2 compatible router
        string memory _name, // Token name
        string memory _symbol, // Token symbol
        uint256 _totalSupply, // Total supply of Token
        uint256 _lockedLiquidity, // Amount of tokens (to be locked into liquidity)
        uint256 _pairedETH, // Amount of ETH to pair with
        uint8 _txnTax, // Tax on each transaction
        uint8 _maxTxn, // Max % of total supply that can be transferred in a single transaction
        uint8 _maxPos, // Max % of total supply that a single address can hold
        bool _limits // Whether or not to enforce the maxTxn and maxPos limits
    ) external payable returns (address) {
        // Check if the deployer fee was paid
        require(msg.value - _pairedETH >= deployerFee, "!fee");

        require(
            _lockedLiquidity >= (_totalSupply * minLiquidity) / 100,
            "!min_liquidity"
        );
        require(_txnTax <= maxTxnTax, "!max_tax"); // 9% max tax

        // Create Token, approve to router
        Shitcoin t = new Shitcoin(
            _uniV2Router,
            _name,
            _symbol,
            _totalSupply,
            _lockedLiquidity,
            _txnTax,
            _maxTxn,
            _maxPos,
            _limits,
            msg.sender,
            taxRecipient
        );
        t.approve(_uniV2Router, _lockedLiquidity);

        // Lock up liquidity on Uniswap, and send to address(0) address
        IUniswapV2Router02(_uniV2Router).addLiquidityETH{value: _pairedETH}(
            address(t),
            _lockedLiquidity,
            _lockedLiquidity,
            _pairedETH,
            address(0x42), // Answer to the universe and everything
            block.timestamp
        );

        emit NewToken(
            address(t),
            _totalSupply,
            _lockedLiquidity,
            msg.sender,
            _pairedETH
        );

        return address(t);
    }

    function changeTaxRecipient(address _newRecipient) external onlyOwner {
        taxRecipient = _newRecipient;
    }

    function changeMaxTxnTax(uint8 _newMaxTxnTax) external onlyOwner {
        maxTxnTax = _newMaxTxnTax;
    }

    function changeMinLiquidity(uint8 _newMinLiquidity) external onlyOwner {
        minLiquidity = _newMinLiquidity;
    }

    function changeDeployerFee(uint256 _newFee) external onlyOwner {
        deployerFee = _newFee;
    }

    function withdrawStuckToken(address _token, address _to)
        external
        onlyOwner
    {
        require(_token != address(0), "_token address cannot be 0");
        uint256 _contractBalance = IERC20(_token).balanceOf(address(this));
        IERC20(_token).transfer(_to, _contractBalance);
    }

    function withdrawStuckEth(address toAddr) external onlyOwner {
        (bool success, ) = toAddr.call{value: address(this).balance}("");
        require(success);
    }
}
