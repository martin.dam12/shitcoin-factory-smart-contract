// SPDX-License-Identifier: MIT
pragma solidity ^0.8.24;

import "./Shitcoin.sol";

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);

    /**
     * @dev Returns the value of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the value of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves a `value` amount of tokens from the caller's account to `to`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address to, uint256 value) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets a `value` amount of tokens as the allowance of `spender` over the
     * caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 value) external returns (bool);

    /**
     * @dev Moves a `value` amount of tokens from `from` to `to` using the
     * allowance mechanism. `value` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address from, address to, uint256 value) external returns (bool);
}

contract ShitcoinFactory is Ownable {
    /*//////////////////////////////////////////////////////////////
                               Constants
    //////////////////////////////////////////////////////////////*/

    /*//////////////////////////////////////////////////////////////
                               Variables
    //////////////////////////////////////////////////////////////*/

    address public taxRecipient = 0x65c2156DddC5808f10f6656f0aF50c76F4D4b7C3; // Turbo

    uint256 public deployerFee = 0.02 ether; // 0.02 ETH

    uint8 public maxTxnTax = 9; // 9% max tax

    uint8 public minLiquidity = 80; // at least 80% of the max supply must be locked in liquidity

    address public deadAddress = address(0x42); // Answer to the universe and everything

    /*//////////////////////////////////////////////////////////////
                                Events
    //////////////////////////////////////////////////////////////*/

    // Deployment
    event NewToken(
        address indexed token,
        uint256 totalSupply,
        uint256 lockedLiquidity,
        address deployer,
        uint256 initialEthLiquidity
    );

    constructor() Ownable(msg.sender) {}

    function deploy(
        address _uniV2Router, // Uniswapv2 compatible router
        string memory _name, // Token name
        string memory _symbol, // Token symbol
        uint256 _totalSupply, // Total supply of Token
        uint256 _lockedLiquidity, // Amount of tokens (to be locked into liquidity)
        uint256 _pairedETH, // Amount of ETH to pair with
        uint8 _txnTax, // Tax on each transaction
        uint8 _maxTxn, // Max % of total supply that can be transferred in a single transaction
        uint8 _maxPos, // Max % of total supply that a single address can hold
        bool _limits // Whether or not to enforce the maxTxn and maxPos limits
    ) external payable returns (address) {
        // Check if the deployer fee was paid
        require(msg.value - _pairedETH >= deployerFee, "!fee");

        require(
            _lockedLiquidity >= (_totalSupply * minLiquidity) / 100,
            "!min_liquidity"
        );
        require(_txnTax <= maxTxnTax, "!max_tax"); // 9% max tax

        // Create Token, approve to router
        Shitcoin t = new Shitcoin(
            _uniV2Router,
            _name,
            _symbol,
            _totalSupply,
            _lockedLiquidity,
            _txnTax,
            _maxTxn,
            _maxPos,
            _limits,
            msg.sender,
            taxRecipient
        );
        t.approve(_uniV2Router, _lockedLiquidity);

        // Lock up liquidity on Uniswap, and send to address(0) address
        IUniswapV2Router02(_uniV2Router).addLiquidityETH{value: _pairedETH}(
            address(t),
            _lockedLiquidity,
            _lockedLiquidity,
            _pairedETH,
            address(deadAddress), // Answer to the universe and everything
            block.timestamp
        );

        emit NewToken(
            address(t),
            _totalSupply,
            _lockedLiquidity,
            msg.sender,
            _pairedETH
        );

        return address(t);
    }

    function changeTaxRecipient(address _newRecipient) external onlyOwner {
        taxRecipient = _newRecipient;
    }

    function changeMaxTxnTax(uint8 _newMaxTxnTax) external onlyOwner {
        maxTxnTax = _newMaxTxnTax;
    }

    function changeMinLiquidity(uint8 _newMinLiquidity) external onlyOwner {
        minLiquidity = _newMinLiquidity;
    }

    function changeDeadAddress(address _deadAddress) external onlyOwner {
        deadAddress = _deadAddress;
    }

    function changeDeployerFee(uint256 _newFee) external onlyOwner {
        deployerFee = _newFee;
    }

    function withdrawStuckToken(address _token, address _to) external onlyOwner {
        require(_token != address(0), "_token address cannot be 0");
        uint256 _contractBalance = IERC20(_token).balanceOf(address(this));
        IERC20(_token).transfer(_to, _contractBalance);
    }

    function withdrawStuckEth(address toAddr) external onlyOwner {
        (bool success, ) = toAddr.call{
            value: address(this).balance
        } ("");
        require(success);
    }
}
